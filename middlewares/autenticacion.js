//JWT
var jwt = require('jsonwebtoken');
// SEED
var SEED = require('../config/config').SEED;


// ==============================================
// Verificar usuario - Middleware
// ==============================================
exports.verificaToken = function (req, res, next) {

	var token = req.query.token;

	jwt.verify(token, SEED, (err, decoded) => {

		if (err) {
			return res.status(401).json({
				ok: false,
				mensaje: 'Error, Token no válido',
				errors: err
			});
		}

		req.usuario = decoded.usuario;
		req.hospital = decoded.hospital;

		// res.status(200).json({
		// 	ok: true,
		// 	decoded: decoded
		// });

		next();
	})


}




