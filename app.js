//Requires (Librerias propios o de terceros para su uso)

var express = require('express');
//Establecer conexion con Mongoose
var mongoose = require('mongoose');
//body-parser
var bodyParser = require('body-parser');


// Inicializar variables

var app = express();

//Body parser (middlewares)
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


//Importar Rutas:

var appRoutes = require('./routes/app');
var usuarioRoutes = require('./routes/usuario');
var hospitalRoutes = require('./routes/hospital');
var medicoRoutes = require('./routes/medico');
var loginRoutes = require('./routes/login');

//Conexion a la DDBB:

mongoose.connect('mongodb://localhost:27017/hospitalDB', { useNewUrlParser: true, useUnifiedTopology: true }, (err, res) => {

	if (err) throw err;
	console.log('Base de Datos: \x1b[32m%s\x1b[0m', 'online');

})

//Rutas:
//Declaro lo que se conoce como "middleware" (algo que se ejecuta antes de que se resuelvan otras rutas )
app.use('/usuario', usuarioRoutes);
app.use('/hospital', hospitalRoutes);
app.use('/medico', medicoRoutes);
app.use('/login', loginRoutes);
app.use('/', appRoutes);


// Escuchar peticiones del Express

app.listen(3000, () => {
	console.log('Express server corriendo en el puerto 3000:\x1b[32m%s\x1b[0m', 'online');
})