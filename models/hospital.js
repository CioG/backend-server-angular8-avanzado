
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//creamos el esquema de datos para los hospitales
var hospitalSchema = new Schema({
	nombre: { type: String, required: [true, "El hospital es obligatorio"] },
	img: { type: String, required: false },
// 	Schema.Types.ObjectId,	esto	es	utilizado	para	indicarle	a	Mongoose,	que	ese	campo	es	una	
// relación	con	otra	colección,	y	la	referencia	es	Usuario. Al	final	este	campo	nos	dirá	qué
// usuario	creó	el	registro.
	usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'hospitales' });


//exportamos el modelo de datos:
module.exports = mongoose.model('Hospital', hospitalSchema);