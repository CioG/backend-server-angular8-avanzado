//Requires (Librerias propios o de terceros para su uso)
var express = require('express');
//JWT
var jwt = require('jsonwebtoken');

//bcrypt js ( una sola vía)
var bcrypt = require('bcryptjs');


// Inicializar variables
var app = express();

// SEED

var SEED = require('../config/config').SEED;

var Usuario = require('../models/usuario');

app.post('/', (req, res) => {
	var body = req.body;


	Usuario.findOne({email: body.email }, (err, usuarioDB) => {
		
		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al buscar usuario',
				errors: err
			});
		}
		if (!usuarioDB) {
			return res.status(400).json({
				ok: false,
				mensaje: 'Credenciales incorrectas - email',
				errors: err
			});
		}

		if(!bcrypt.compareSync(body.password, usuarioDB.password)) {
			return res.status(400).json({
				ok: false,
				mensaje: 'Credenciales incorrectas - password',
				errors: err
			});
		}

		//crear un token!!
	  usuarioDB.password = ';)' //Para no enviar el password en la petición, envio "cara feliz"
var token = jwt.sign({usuario: usuarioDB }, SEED, { expiresIn: 14400})  //4 horas de expiración del token



		res.status(200).json({
			ok: true,
			mensaje: 'Login POST correcto',
			usuario: usuarioDB,
			token:token,
			id: usuarioDB._id
		})
	})

})




module.exports = app;

