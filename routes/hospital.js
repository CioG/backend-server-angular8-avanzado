
//Requires (Librerias propios o de terceros para su uso)
var express = require('express');

//Middleware creado
var mdAuthenticacion = require('../middlewares/autenticacion');

// Inicializar variables
var app = express();

var Hospital = require('../models/hospital');

// ==============================================
// Obtener todos los hospitales
// ==============================================

app.get('/', (req, res) => {

	Hospital.find({})
		.exec(
			(err, hospitales) => {
				if (err) {
					return res.status(500).json({
						ok: false,
						mensaje: 'Error en BBDD al obtener los hospitales',
						errors: err
					});
				}
				res.status(200).json({
					ok: true,
					hospitales: hospitales
				})
			}
		)
})

// ==============================================
// Actualizar hospital
// ==============================================

app.put('/:id',  mdAuthenticacion.verificaToken, (req, res) => {

	var id = req.params.id;
	var body = req.body;

	Hospital.findById(id, (err, hospital) => {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al buscar hospital',
				errors: err
			});
		}
		if (!hospital) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El hospital con el id ' + id + ' no existe',
				errors: { message: 'No existe un hospital con ese ID' }
			});
		}

		hospital.nombre = body.nombre;
		hospital.usuario = req.usuario._id;


		hospital.save((err, hospitalGuardado) => {

			if (err) {
				return res.status(400).json({
					ok: false,
					mensaje: 'Error en BBDD al actualizar hospital',
					errors: err
				});
			}

			res.status(200).json({
				ok: true,
				hospital: hospitalGuardado
			});
		});
	});
});

// ==============================================
// Crear nuevo hospital
// ==============================================

app.post('/', mdAuthenticacion.verificaToken, (req, res) => {


	var body = req.body;

	var hospital = new Hospital({
		nombre: body.nombre,
		usuario: req.usuario._id
	});

	hospital.save((err, hospitalGuardado) => {
		if (err) {
			return res.status(400).json({
				ok: false,
				mensaje: 'Error en BBDD al crear Nuevo hospital',
				erros: err
			});
		}
		res.status(201).json({
			ok: true,
			hospital: hospitalGuardado
		})
	})

});



// ==============================================
// Borrar  Hospital
// ==============================================

app.delete('/:id', mdAuthenticacion.verificaToken, (req,res )=> {
	var id = req.params.id;
	Hospital.findByIdAndRemove(id, (err, hospitalBorrado)=> {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al BORRAR nuevo hospital',
				errors: err
			});
		}

		if (!hospitalBorrado) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El hospital no existe con ese ID',
				errors: { message: 'No existe hospital con ese ID' }
			});
		}


		res.status(200).json({
			ok: true,
			hospital: hospitalBorrado
		});

	})



})





module.exports = app;