
//Requires (Librerias propios o de terceros para su uso)
var express = require('express');

//bcrypt js ( una sola vía)
var bcrypt = require('bcryptjs');

//JWT
var jwt = require('jsonwebtoken');

//Middleware creado
var mdAuthenticacion = require('../middlewares/autenticacion');

// Inicializar variables
var app = express();

var Usuario = require('../models/usuario');

// ==============================================
// Obtener todos los usuarios
// ==============================================

app.get('/', (req, res, next) => {

	Usuario.find({}, 'nombre email img role')
		.exec(
			(err, usuarios) => {
				if (err) {
					return res.status(500).json({
						ok: false,
						mensaje: 'Error en BBDD cargando usuarios',
						errors: err
					});
				}
				res.status(200).json({
					ok: true,
					usuarios: usuarios
				});
			});
});


// ==============================================
// Actualizar usuario
// ==============================================

app.put('/:id',  mdAuthenticacion.verificaToken, (req, res) => {

	var id = req.params.id;
	var body = req.body;

	Usuario.findById(id, (err, usuario) => {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al buscar usuario',
				errors: err
			});
		}
		if (!usuario) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El usuario con el id ' + id + ' no existe',
				errors: { message: 'No existe un usuario con ese ID' }
			});
		}

		usuario.nombre = body.nombre;
		usuario.email = body.email;
		usuario.role = body.role;

		usuario.save((err, usuarioGuardado) => {

			if (err) {
				return res.status(400).json({
					ok: false,
					mensaje: 'Error en BBDD al actualizar usuario',
					errors: err
				});
			}

			usuarioGuardado.password = ':)';
			res.status(200).json({
				ok: true,
				usuario: usuarioGuardado
			});
		});
	});
});

// ==============================================
// Crear nuevo usuario
// ==============================================
//Pondre la autorizacion como segundo parámetro
app.post('/', mdAuthenticacion.verificaToken, (req, res) => {
	//Para que esto funcione necesito tener el bodyparser, si no dará undefined
	var body = req.body;

	var usuario = new Usuario({
		nombre: body.nombre,
		email: body.email,
		// var hash = bcrypt.hashSync("B4c0/\/", salt); segun docu de bcrypt
		password: bcrypt.hashSync(body.password, 10),
		img: body.img,
		role: body.role
	});


	usuario.save((err, usuarioGuardado) => {

		if (err) {
			return res.status(400).json({
				ok: false,
				mensaje: 'Error en BBDD al crear nuevo usuario',
				errors: err
			});
		}

		res.status(201).json({
			ok: true,
			usuario: usuarioGuardado,
			usuariotoken:req.usuario
		});
	})
});


// ==============================================
// Borrar  usuario
// ==============================================

app.delete('/:id', mdAuthenticacion.verificaToken, (req,res )=> {
	var id = req.params.id;
	Usuario.findByIdAndRemove(id, (err, usuarioBorrado)=> {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al BORRAR nuevo usuario',
				errors: err
			});
		}

		if (!usuarioBorrado) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El usuario un usuario con ese ID',
				errors: { message: 'No existe un usuario con ese ID' }
			});
		}


		res.status(200).json({
			ok: true,
			usuario: usuarioBorrado
		});

	})



})


module.exports = app;