
//Requires (Librerias propios o de terceros para su uso)
var express = require('express');

//Middleware creado
var mdAuthenticacion = require('../middlewares/autenticacion');

// Inicializar variables
var app = express();

var Medico = require('../models/medico');

// ==============================================
// Obtener todos los medicos
// ==============================================

app.get('/', (req, res) => {

	Medico.find({})
		.exec(
			(err, medicos) => {
				if (err) {
					return res.status(500).json({
						ok: false,
						mensaje: 'Error en BBDD al obtener los medicos',
						errors: err
					});
				}
				res.status(200).json({
					ok: true,
					medicos: medicos
				})
			}
		)
})

// ==============================================
// Actualizar medico
// ==============================================

app.put('/:id',  mdAuthenticacion.verificaToken, (req, res) => {

	var id = req.params.id;
	var body = req.body;

	Medico.findById(id, (err, medico) => {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al buscar medico',
				errors: err
			});
		}
		if (!medico) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El medico con el id ' + id + ' no existe',
				errors: { message: 'No existe un medico con ese ID' }
			});
		}

		medico.nombre = body.nombre;
		medico.usuario = req.usuario._id;
		medico.hospital = body.hospital;


		medico.save((err, medicoGuardado) => {

			if (err) {
				return res.status(400).json({
					ok: false,
					mensaje: 'Error en BBDD al actualizar medico',
					errors: err
				});
			}

			res.status(200).json({
				ok: true,
				medico: medicoGuardado
			});
		});
	});
});

// ==============================================
// Crear nuevo medico
// ==============================================

app.post('/', mdAuthenticacion.verificaToken, (req, res) => {


	var body = req.body;

	var medico = new Medico({
		nombre: body.nombre,
		usuario: req.usuario._id,
		hospital: body.hospital
	});

	medico.save((err, medicoGuardado) => {
		if (err) {
			return res.status(400).json({
				ok: false,
				mensaje: 'Error en BBDD al crear Nuevo medico',
				erros: err
			});
		}
		res.status(201).json({
			ok: true,
			medico: medicoGuardado
		})
	})

});



// ==============================================
// Borrar  Medico
// ==============================================

app.delete('/:id', mdAuthenticacion.verificaToken, (req,res )=> {
	var id = req.params.id;
	Medico.findByIdAndRemove(id, (err, medicoBorrado)=> {


		if (err) {
			return res.status(500).json({
				ok: false,
				mensaje: 'Error en BBDD al BORRAR nuevo medico',
				errors: err
			});
		}

		if (!medicoBorrado) {
			return res.status(400).json({
				ok: false,
				mensaje: 'El médico no existe con ese ID',
				errors: { message: 'No existe médico con ese ID' }
			});
		}


		res.status(200).json({
			ok: true,
			medico: medicoBorrado
		});

	})



})





module.exports = app;